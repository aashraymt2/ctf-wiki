# GDB -- The GNU Debugger
GDB is the go-to linux machine level debugger. It lets us watch what actually happens in memory/registers.

## Installation
Bare-bones gdb should already be installed. Replace it with the multi-arch version.
```
sudo apt install -y gdb-multiarch
sudo ln -f -s $(which gdb-multiarch) $(which gdb)
```

All the major gdb addons are roughly feature identical, although GEF is the recent team go-to.

Add all the addons using the following article, but substitute `/usr/bin/` for `~/.local/bin`
- https://infosecwriteups.com/pwndbg-gef-peda-one-for-all-and-all-for-one-714d71bf36b8

## Usage
Learning GDB is one of those things that's circular with learning assembly and C compiler options really well.

Python has a gdb clone called pdb, learning pdb or ipython's ipdb is probably a better way to get comfortable with the gdb controls.

Articles:
- https://developers.redhat.com/blog/2021/04/30/the-gdb-developers-gnu-debugger-tutorial-part-1-getting-started-with-the-debugger#starting_gdb
- https://www.cs.umd.edu/~srhuang/teaching/cmsc212/gdb-tutorial-handout.pdf

Videos:
- https://www.youtube.com/watch?v=sCtY--xRUyI
- https://www.youtube.com/watch?v=Xqmr-8FRchw&ab_channel=LiveUnderflow

Example Problems:
- Just compile a C hello-world and step through it, make it do more stuff and see what it looks like in GDB
