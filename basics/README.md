# Basics
Generally the things 'required' for starting with CTF's are the same as the
basic tooling/concepts for anything cyber related.
- Linux
- Python and/or C

Additionally helpful but not required:
- Network programming knowledge
- OS fundamentals
- virtualization
- docker -- for sharing system images/network setups
- Other Common Tools

## Linux
Linux is the go to operating system for everything cyber, it lets the user do
pretty much anything even if that thing will ruin the machine.

As of 2021 ish, Ubuntu 18.04 is the most common/beginner friendly linux distro.

There are a few options for running a linux system for playing in a CTF
1. Native Install or Virtual Machine (nearly the same setup)
    - https://www.virtualbox.org/
    - https://www.howtogeek.com/693588/how-to-install-linux/
    - https://www.guru99.com/install-linux.html
3. Docker Container (for isolation from a host linux machine)
    - https://docs.docker.com/get-docker/
    - `docker run -it ubuntu:18.04`
4. Windows Subsystem for Linux (WSL)
    - https://ubuntu.com/wsl
    - To use other distros, just install from the windows store

- https://wiki.bi0s.in/basics/intro/

## Python Language
Comes default on Ubuntu, the website has Windows distributions.
- https://www.python.org/
- https://docs.python.org/3/

If you have a tendency to use python's shell a lot, try the IPython shell instead.
The IPython shell has a lot of nice add-ons like tab completion and syntax highlighting.
```
# Jedi is the tab-completion engine and breaks in a recent version
python3 -m pip install -U ipython "jedi<0.18"
```

## Other Common Tools
- [pwntools](tools/pwntools.md)
