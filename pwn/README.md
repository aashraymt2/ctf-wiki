# pwn -- Binary Exploitation
## Overview
Binary exploitation is the category of problems that largely involve abusing computers at the assembly/machine and operating system levels.

This section goes hand-in-hand with RE.

Some introductions:
- https://wiki.bi0s.in/pwning/intro/
- https://ctf101.org/binary-exploitation/overview/

### Approaching PWN
A general approach to PWN can be summarized as:
1. Learning machine level operations + debugging
2. Exploring memory corruptions and the implications
3. Learning stack exploitation methods/primitives such as stack smashing and return-oriented-programming
4. Learning about system defenses and how they interact with the primitives
5. Learning the more advanced heap based exploit techniques
6. Learning pwn again with automation and advanced tooling
7. Learning pwn again with the nuances of specific platforms (ie windows vs linux)

Links:
- https://ctf101.org/binary-exploitation/overview/ -- Good intro to machine operations
- https://wiki.bi0s.in/pwning/roadmap/ -- Good overview of what order to learn exploits/concepts in
- https://github.com/RPISEC/MBE -- Example of a full class on this, mainly in gdb/x86

Videos:
- https://www.youtube.com/watch?v=akCce7vSSfw&ab_channel=LiveOverflow
- https://pwn.college/
    - A whole lecture series with labs on pwn

### Tooling
These are the go-to tools for pwn, not every problem will need every tool and not every tool can solve every problem.

- Disassemblers
    - Ghidra
    - IDA
    - Binary Ninja (binja)
- General Utilities
    - [pwntools](/basics/tools/pwntools.md) -- an absolute unit of a toolbox
    - Linux binutils package
- Debuggers
    - [gdb](/basics/tools/gdb.md) -- Linux machine debugger
    - LLDB -- LLVM project debugger
    - WinDbg -- the Windows debugger
- Instrumentation Tools
    - LIEF
    - LLVM -- fsanitize options
- Emulation Tools
    - angr
    - Qemu
    - Qiling
    - Renode.io
- Fuzzers
    - AFL/AFL++/AFL-unicorn
    - LLVM -- libfuzzer

### Machine Level Operation + Debugging
Learn the lifecycle of C to binary to running program.
Learn how things generally look in running process memory space.
Learn to skim assembly and basic debugging with objdump and gdb.

Videos:
- https://www.youtube.com/playlist?list=PL-ymxv0nOtqrCRbctQYA62VGyWkko6rWC
    - Guy in these videos is Yan, previous captain of ShellPhish / Order of the Overflow ctf teams
    - This is probably the best high level intro available from a CTF pwn perspective

Articles:
- https://ctf101.org/binary-exploitation/overview/
    - The first few sections on the overview here give a good set of definitions for machine concepts:
- https://azeria-labs.com/writing-arm-assembly-part-1/
    - An exploit-centric ARM32 tutorial, it's a lot easier to learn than x86 and the concepts translate.
- https://github.com/RPISEC/MBE
    - RPISEC's Modern Binary Exploitation (MBE) class has an okay intro in the first lecture

Examples:
- It's easy to start by compiling a C program and the disassembling it to see what happens/changes. Try with `objdump -d`
- Watch things happen in assembly, again ARM32 is easier to learn here (do it with Qemu or on a raspi) but things are generally similar across achitectures.
- read through `/proc/x/maps` for a process and look how things are loaded
- TODO: make some examples with make or somethings
- TODO: I heard the first few challenges on PicoCTF are basically a concept review for this

### Memory Corruption + Implications
TL;DR -- Machine's don't care about intent, all data looks executable if it ends up in the right spot

https://pwn.college/modules/memory

TODO: this section could use more

## Logic Errors + Type Overflows
Language / machine / implementation specific

Articles/Presentations:
- https://en.wikipedia.org/wiki/Memory_corruption

Videos:
- https://www.youtube.com/watch?v=z_XOhfsVKnU

Examples:
- [logic-error](/pwn/examples/logic-error/logic-error.c)

## Stack Based Techniques
### Buffer Overflows
Data being written/copied out of bounds, typically within arrays/strings.

Articles/Presentations:
- https://ctf101.org/binary-exploitation/what-is-the-stack/
- https://insecure.org/stf/mudge_buffer_overflow_tutorial.html

Videos:
- https://www.youtube.com/watch?v=qSnPayW6F7U&ab_channel=TheCyberMentor
    - x86 stack centric

Examples:
- [visualized-overflow](/pwn/examples/visualized-overflow/visualized-overflow.c)
- https://www.youtube.com/watch?v=2ZZPwwXOH08&ab_channel=JohnHammond

### Stack Smashing
Stack based buffer overflows allow us to control other data on the stack, including stack frames.

This is typically a very x86/x86_64 centric topic due to the well-defined stack frame.
In architectures like ARM the stack frame is a little less rigidly structured.

Smashing the stack leads to being able to control stack variables and the return address.

Endianness is usually an annoyance when trying to manipulate the stack.

Articles/Presentations:
- https://docs.google.com/presentation/d/1_Zs7s7O_VqXd8prv0GIjUT993qL3KgjVby8qC0Ixs_w/edit#slide=id.p
- https://insecure.org/stf/smashstack.html
- https://wiki.bi0s.in/pwning/stack-overflow/smash-the-stack/

Videos:
- https://www.youtube.com/watch?v=PVx1hUlMxtQ&ab_channel=pwn.college

Examples:
- [stack-smash](/pwn/examples/stack-smash/stack-smash.c)

### Return Control / ret2win
This technique is synonymous with 'stack smashing' in most literature.
It is more appropriate to call this ret2win or return pointer control.

Stack canaries are the mitigations specifically targeting stack smashes.

Articles/Presentations:
- https://ir0nstone.gitbook.io/notes/types/stack/ret2win
- https://wiki.bi0s.in/pwning/stack-overflow/smash-the-stack/

Videos:
- https://www.youtube.com/watch?v=ubpBELyn0YM&ab_channel=CryptoCat

Examples:
- [ret2win](/pwn/examples/ret2win/ret2win.c)
- https://ropemporium.com/challenge/ret2win.html/

### De Bruijn Sequences / pwntools cyclic example
De Bruijn Sequences are long sequences of characters where each substring of a given length (usually 4) is unique.
These can be used to calculate offsets for return control super easy.
Pwntools has a subtool `cyclic` for generating and searching these sequences.

https://docs.pwntools.com/en/stable/util/cyclic.html

Re-run ret2win with this as the input, run a lookup on the resulting EIP/RIP value.

Examples:
- [ret2win-cyclic](/pwn/examples/ret2win/ret2win-cyclic.py)

### Thinking past single `void win(void)` payloads
To this point, and with ret2shellcode, we have mainly explored exploit techniques that result in being able to call an arbitrary `void func(void)` within our program. Now we'll consider a few evolutions of these techniques that will lead us into our next common problem categories. These concepts may seem simple or obvious but they are critical to being able to solve non-trivial binary exploitation problems and represent a large increase in problem difficulty.

#### Controlling uninitialized variables in a target function's stack.
We need to remember back to our first programming classes when we were told to always init variables before using them. Sometimes target binaries will have stack variables in functions that we can control from our initial overflow. This is possible because the stack is not cleaned when rsp/rbp are moved.

TODO: links/videos/example

#### Multiple return addresses in our payload.
So far, all of our expoit techniques have relied on returning into a function, but what happens if we keep adding addresses after our target address? The answer is that our target function will return into the next address! This allows us to chain execution through multiple functions that may not otherwise be in a call path.

TODO: links/videos

[multiple-returns](pwn/examples/multiple-returns.c)

#### Returning into `vuln()` again for multiple payload chaining. (ret2vuln)
As mentioned above, we can just keep returning into any function we want, so why not call our vulnerable function again? This allows us to effectively turn the target program into a payload repl. We would do this in situations in which we want to use one vulnerable input function to gather runtime information like variable values and library addresses, and then use these in later payloads for accomplishing the final goal.

Sometimes this is also seen as problems where we need to pivot from a `vuln1` that lets us control only the return address to a `vuln2` where we can do a full rop chain or leak other information.

TODO: links/videos

[ret2vuln](/pwn/examples/ret2vuln/ret2vuln.c)

TODO: HTB cyber santa pwn/naughty_list

#### Gadgets
So far we've just returned into the starting address of functions. This allows us to have relatively C level behavior with the target function allocating and deallocating its own stack then returning. However, we only really need that these functions `ret`. Since we can adjust our 'return' address to be any executable code, we can return to the middle of another procedure. A series of assembly instructions ending in `ret` is generally defined as a gadget if it is of use to us in this way.

When using gadgets, it is best to be aware of any stack cleanup that the original procedure is expecting to do, otherwise you will end up wiping your own stack overflow.

One of the common CTF gadgets is an otherwise unreachable call to `execve`  or `system` that results in a shell.

TODO: examples/links/graphics
- https://ir0nstone.gitbook.io/notes/types/stack/return-oriented-programming/gadgets

[simple-gadget](/pwn/examples/simple-gadget/simple-gadget.c)

#### Gadgets + stack values to pass parameters
Now that we have gadgets, which are just useful assembly operations, we combine their use with our stack. If we can use gadgets in order to pop values off the stack into the correct registers then we can properly format calls to functions as if we have just called them from the program normally. This breaks us free of the `void func(void)` pattern. Worth mentioning here is that we can use the strings from not only our target program as possible `pop` addresses but also strings from all loaded libraries.

It is also helpful to have a target function and its expected argument layout in mind before looking for gadgets as it can be quickly overwhelming to start from the gadget side.

TODO: examples/links/graphics

[gadget-params](/pwn/examples/gadget-params/gadget-params.c)

### ret2libc
ret2win is not limited to functions in your binary.
It can be controlled into any loaded asm, including libraries, but requires finding some point of reference for the load address of the target library.
libc aso happens to have enough to call `system` with `/bin/sh`, this is called 'one-gadget'. This may require calling multiple functions/gadgets and getting arguments to registers before calling, which is more 'ROP' than ret2libc.

ASLR is the defense meant to mitigate ret2libc (and rop).

Articles/Presentations:
- https://bufferoverflows.net/ret2libc-exploitation-example/
- https://0xrick.github.io/binary-exploitation/bof6/

Videos:
- https://www.youtube.com/watch?v=hJ8IwyhqzD4&t=514s&ab_channel=Computerphile

Examples:
- TODO: put together a `printf` leak to onegadget example
- https://ropemporium.com/
    - Running in GDB disables ASLR
    - Can also disable it at the host level but don't do that

### Return-Oriented-Programming (ROP)
In ret2libc, we added arguments and used the return pointer to effectively call a function.
Gadgets are bits of assembly that end in `ret`urning instructions.
We can treat gadgets as functions that work more at the machine level.
If we keep adding more gadget stack frames to our overflown stack, we have a rop chain.
This effectively creates a assembly program in reverse-frame order.

ASLR is also meant to mitigate ROP.

Articles/Presentations:
- https://ir0nstone.gitbook.io/notes/types/stack/return-oriented-programming
- https://wiki.bi0s.in/pwning/rop/static/

Videos:
- https://www.youtube.com/watch?v=yS9pGmY_xuo&ab_channel=PicoCetef

Examples:
- https://ropemporium.com/

Tooling:
- TODO: ropper

### TODO: ret2csu

## Shellcode
Shellcoding is the art of injecting code into a program, usually during exploitation, to get it to carry out actions desired by the attacker [[1](https://en.wikipedia.org/wiki/Shellcode)].

Generally shellcode boils down to two steps:
1. Injection of executable code into the program
2. Executing the injected code

Articles/Presentations:
- https://ir0nstone.gitbook.io/notes/types/stack/shellcode
- https://wiki.bi0s.in/pwning/stack-overflow/return-to-shellcode/
- https://pwn.college/modules/shellcode

Videos:
- https://www.youtube.com/playlist?list=PL-ymxv0nOtqomtHqMqqgpuvWdVSs9NCBK

### syscalls
Syscalls are 'magic' functions that get the kernel to do something for a userspace program. ie open a file. Considering that everything has to be done thorugh the kernel at some point, these are a very compatc way to interact with the underlying OS.

In Linux, syscalls are made by setting correct register arguments and hitting a architecture-specific syscall instruction. In windows syscalls must be done via the system dll's that wrap the calls, which is more similar to the glibc wrapping structure. glibc also provides a minimal syscall function that makes developing shellcode in C very easy.

Articles/Presentations:
- https://linuxhint.com/linux_system_call_tutorial_c/
- https://chromium.googlesource.com/chromiumos/docs/+/master/constants/syscalls.md

Videos:
- TODO

### TODO: How to make shellcode
Go back and find the presentation for:
- raw assembly
- `-nostartfiles` and syscalls in c
- pwntools shellcraft
- running/testing

### ret2shellcode
Compared to a ret2win problem; if you can ret, then why not bring your own `win()`?

Articles/Presentations:
- https://wiki.bi0s.in/pwning/stack-overflow/return-to-shellcode/

Videos:
- TODO

Examples:
- [ret2shellcode](/pwn/examples/ret2shellcode/ret2shellcode.c)
- TODO: HTB 2021 Cyber Santa pwn/sleigh

### TODO: nop sledding

### TODO: handling shellcode contraints
- non-null
- shellcode decoders/unpackers
    - https://www.cs.jhu.edu/~sam/ccs243-mason.pdf
    - see defcon 27 presentation

### TODO: living off the land
- https://docs.broadcom.com/doc/istr-living-off-the-land-and-fileless-attack-techniques-en

### TODO: ret2dl_resolve

### TODO: code cave injection

## Heap Based Techniques
### Heap Smashing
Buffer overflow but on the heap, usually results in corrupting objects or other alloc'ed memory to be used elsewhere.

Articles/Presentations:
- https://en.wikipedia.org/wiki/Heap_overflow
- https://ctf101.org/binary-exploitation/heap-exploitation/

Videos:

Examples:

### Use-After-Free / Double Free
- https://cwe.mitre.org/data/definitions/416.html
- https://cwe.mitre.org/data/definitions/415.html
- TL:DR We can control data that the software really shouldn't be using, usually in the heap

### Stack Pivot
- https://failingsilently.wordpress.com/2018/04/17/what-is-a-stack-pivot/
- https://ir0nstone.gitbook.io/notes/types/stack/stack-pivoting
- http://neilscomputerblog.blogspot.com/2012/06/stack-pivoting.html
- https://bananamafia.dev/post/binary-rop-stackpivot/

### Heap TODO
- controlling malloc
- clobbering
- vtable overrides
- vtable override to iterate through rop gadgets
    - https://securitylab.github.com/research/CVE-2020-6449-exploit-chrome-uaf/

## Memory Leaks and Variadic Arguments
- TODO: printf
- TODO: other variadics
- TODO: /proc/self/maps
- TODO: fork leaks

## Race Conditions
- https://hackernoon.com/time-of-check-to-time-of-use-toctou-a-race-condition-99c2311bd9fc
- TL:DR -- If we can modfiy memory you're about to use then we might be able to cause a logic bug or overflow
- TOCTOU

## Type Confusion
- https://hackingportal.github.io/Type_Confusion/type_confusion.html    


## Binary Defenses -- TODO: bypasses, merge into correct sections
### Stack Canaries
Do a verification that the stack frame has not been completely smashed on function return
- https://ctf101.org/binary-exploitation/stack-canaries/

### No-Execute (NX)/Data Execution Prevention (DEP) + memory permissions
Don't allow executable stacks / data regions
- https://ctf101.org/binary-exploitation/no-execute/

### ASLR
- If code is compiled position-independent (relative offsets), then let it be loaded in at 'random' addr's
- randomization does not occur on forks or clones, only on library loading
- https://ctf101.org/binary-exploitation/address-space-layout-randomization/

### Relocation Read-Only (RELRO)
Make some things read only (like strings)
- https://ctf101.org/binary-exploitation/relocation-read-only/

### Shadow Stack / Control-Flow Enforcement
Have a second stack to verify your stack
- https://en.wikipedia.org/wiki/Shadow_stack

### SMEP/SMAP
Keep the kernel from directly running user-space code.
Keep the kernel from direcly accessing userspace memory under most conditions
- https://wiki.osdev.org/Supervisor_Memory_Protection

## Automation and More Tooling
- TODO: the list of pwnables from github here
- static code/bin analysis
    - TODO
- pwntools
    - https://github.com/Gallopsled/pwntools-tutorial
- LIEF
    - https://github.com/parikhakshat/autoharness -- source for this is crap if someone could re-write it
    - TODO: auto harness example
- fuzzing
    - TODO: link to fuzzing training
    - https://www.guru99.com/fuzz-testing.html
    - https://fuzzing-project.org/tutorial1.html
    - https://github.com/google/fuzzing/blob/master/tutorial/libFuzzerTutorial.md
    - https://forallsecure.com/resources/ultimate-guide-to-fuzz-testing

## Platform Specifics
- Windows
    - Program Header Block Parsing for shellcode
- Linux
- Android
- iPhone
