//  gcc -nostartfiles -fno-stack-protector -no-pie
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>

char* sh_path = "/bin/sh";
char* sh = "sh";
char* sh_flag = "-c";
char* date_path = "/bin/date";

void obvious_gadgets(){
    // Here's enough gadgets to pop into pretty much every argument register
    asm("pop %rax; ret");
    asm("pop %rbx; ret");
    asm("pop %rcx; ret");
    asm("pop %rdx; ret");
    asm("pop %rsi; ret");
    asm("pop %rdi; ret");
    asm("pop %r8; ret");
    return;
}

void run_date()
{
    system(date_path);
}

void vuln()
{
    char buf[32];
    read(0, buf, 0x100);
}

int main()
{
    vuln();
    run_date();
    exit(0);
}
