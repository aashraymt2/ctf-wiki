//  gcc -nostartfiles -fno-stack-protector -no-pie
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

void win_1()
{
    char mesg[2] = "w";
    puts(mesg);
}

void win_2()
{
    char mesg[2] = "i";
    puts(mesg);
}

void win_3()
{
    char mesg[2] = "n";
    puts(mesg);
}

void vuln()
{
    char buf[32];
    read(0, buf, 100);
}

void _start()
{
    vuln();
    exit(0);
}
