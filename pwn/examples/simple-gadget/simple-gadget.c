//  gcc -nostartfiles -fno-stack-protector -no-pie
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

void impossible_win()
{
    volatile int i = 0;
    if (i == 0) {
        return;
    }
    execl("/bin/sh", "sh", "-c", "sh", (char *) 0);
    // This code is unreachable in any 'correct' execution
    return;
}

void vuln()
{
    char buf[32];
    read(0, buf, 100);
}

void _start()
{
    vuln();
    exit(0);
}
