// Minified version of hackasat2's disassembly/treeintheforest problem
#include <stdio.h>
#include <stdlib.h>

// Program globals
// `objdump -D` then look for "lock"
unsigned char lock = 0x10;
unsigned char entries[5] = {1, 2, 3, 4, 5};

int main()
{
    int input = 0;
    int index = 0;
    unsigned char target = 0x42;
    do
    {
        printf("0. quit\n");
        printf("1. increment\n> ");

        scanf("%i", &input);

        if (input == 1)
        {
            printf("Select an entry 1-5:\n> ");
            scanf("%i", &index);
            ++entries[index];
            printf("Entry %i is now %x\n\n", index, entries[index]);
        }
    
        if (lock == target)
        {
            puts("win");
            exit(1);
        }

    } while (input != 0);
}
