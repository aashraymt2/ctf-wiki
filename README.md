# ctf-wiki
(another) Collection of CTF related concepts and tools.

This wiki does not to be a project to re-create a complete knowledge base but should serve as a place to glob useful links and tools together.
Please prefer (reputable) open internet articles and links to writing a new page.

Please keep any non-opensource opinions and information out of this wiki, don't wany anyone to get in trouble.

Ideally each concept would have at least one of each of the following:
- article/presentation
- video presentation
- List of tools that assist
- example ctf challenges
- additional sources for in-depth coverage

## Intro to CTF's
CTF's are technical challenges, usually related cyber security and vulnerabilities.
Generally CTF's are "jeapoardy style" where teams answer progressively harder questions from the following categories:
- Reverse Engineering
- Binary Exploitation
- Cryptography
- Web Exploitation
- Forensic Analysis

Here are a few of the most popular CTF's each year
- [Defcon CTF](https://defcon.org/html/links/dc-ctf.html)
- [Google CTF](https://capturetheflag.withgoogle.com/)
- [Hackasat](https://www.hackasat.com/)
- [CSAW](https://ctf.csaw.io/)
- [PicoCTF](https://picoctf.org/)

Additional Info / Descriptions
- https://wiki.bi0s.in/#what-is-a-ctf
- https://ctf101.org/

## Where to start in this wiki
Prerequisite Knowledge
- Checkout [the basics](basics/README.md) for getting started with the absolute minumum common tooling

Specific Category Sections
- [pwn](pwn/README.md)
- [re](re/README.md)
- [crypto](crypto/README.md)
- [forensics](forensics/README.md)
- [web](web/README.md)

Also check out the additional links below.

## Links + TODO's
Other wikis
- https://wiki.bi0s.in/
- https://ctf101.org/
- https://github.com/christophermorrison/ctf-training -- original version of this, need to merge and condense
- https://pwn.college/
    - https://www.youtube.com/c/pwncollege/playlists
- https://ir0nstone.gitbook.io/notes/
- https://picoctf.org/resources
- web oriented https://knowledge-base.secureflag.com/vulnerabilities/sql_injection/sql_injection_python.html

Tools
- disassemblers/decompilers
    - https://cloud.binary.ninja/
    - ghidra
    - ida
    - radare2
    - https://cutter.re/
- https://github.com/lief-project/LIEF
- burp suite
- angr
- https://docs.pwntools.com/en/stable/

Meta Lists
- https://github.com/zardus/ctf-tools
- https://www.kali.org/tools/
- https://blogs.nvcc.edu/kdinh/ctftools/
- https://awesomeopensource.com/projects/ctf-tools
- https://project-awesome.org/apsdehal/awesome-ctf


Unsorted
- https://cryptopals.com/
- gitlab/cyberatuc
- ctftime.org
- https://attack.mitre.org/
- https://twitter.com/sec_r0?s=21
- https://margin.re/media/an-opinionated-guide-on-how-to-reverse-engineer-software-part-1.aspx
- https://nora.codes/tutorial/an-intro-to-x86_64-reverse-engineering/
- https://www.hackasat.com/keep-learning
- https://ropemporium.com/

TODOs
- [ ] Would probably be smart to have some ci/cd that checks all the links still 200
- [ ] add a 'getting started' to the discord
- [ ] link to discord?
- [ ] link to ctftime team?
- [ ] common tooling -- setup script, docker, vm formats -- ideally base off or juse use existing VM's for minimal setup
